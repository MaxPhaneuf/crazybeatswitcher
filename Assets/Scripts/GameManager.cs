using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public float heatIncrement = .05f;
    public Manager[] managers;
    public AudioManager audioClips;
    public TimeManager time;
    public StepProvider steps;
    public TextMeshProUGUI bmpText;
    public TextMeshProUGUI styleText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI topScoreText;
    public TextMeshProUGUI speedUp, danger;
    public AudioSource speedUpSound, gameOverSound, missSound;
    public Image negativeFill, positiveFill;
    public GameObject startPanel;
    public List<AudioProvider> providers = new();
    public int currentProviderIndex = 0;
    public int beatStartInSeconds;
    public SpriteRenderer background;
    
    
    
    public Animator waveAnimator;
    public Slider negativeSlider, positiveSlider;
    private const int SecondsUntilBeatChange = 10;
    private bool _active;
    private float _startTime;
    private bool _danger;
    private bool _speedUp;
    private int _currentBmp = 60;
    private Style _style;
    private float _currentHeat = 0;
    private int _currentScore = 0;
    private int _currentTopScore = 0;
    private const float FlashTime = .25f;
    public static GameManager Instance;
    private void Awake()
    {
        managers = FindObjectsOfType<Manager>();
        foreach (var manager in managers)
        {
            if (manager.TryGetComponent(out AudioManager a))
            {
                audioClips = a;
            } else if (manager.TryGetComponent(out TimeManager t))
            {
                time = t;
            } else if (manager.TryGetComponent(out StepProvider s))
            {
                steps = s;
            }
        }

        Instance = this;
        _currentTopScore = PlayerPrefs.GetInt("TopScore");
        topScoreText.text = $"{_currentTopScore:0000000}";
    }

    public void ChangeScore(bool isNegative = false)
    {
        _currentHeat += isNegative ? -heatIncrement : heatIncrement;
        _currentHeat = Mathf.Clamp(_currentHeat, -1, 1);
        positiveSlider.value = float.IsNegative(_currentHeat) ? 0 : _currentHeat;
        negativeSlider.value = !float.IsNegative(_currentHeat) ? 0 : -_currentHeat;
        if(!isNegative)
            _currentScore += _currentBmp + 1 + (int)(_currentHeat * _currentBmp);
        scoreText.text = $"{_currentScore:0000000}";
    }

    public void ChangeStyle(string style)
    {
        styleText.text = style;
    }
    
    public void StartGame()
    {
        startPanel.SetActive(false);
        ChangeBackgroundColor();
        foreach (var manager in managers)
        {
            manager.Init();
        }
        beatStartInSeconds = time.seconds;
        _style = audioClips.PlayRandomClip(providers[currentProviderIndex]);
        bmpText.text = providers[currentProviderIndex].name;
        _active = true;
        steps.SpawnStep(_style);
        _startTime = Time.time;
        waveAnimator.enabled = true;
        AdjustBmpText();
    }

    private void Update()
    {
        if (!_active) return;
        if (_currentHeat <= -1 && !_danger)
        {
            _danger = true;
            StartCoroutine(FlashNegative());
        }

        if (_currentHeat >= 1 && !_speedUp)
        {
            _speedUp = true;
            StartCoroutine(FlashPositive());
        }
        ChangeBeat();
        SpawnStep();
    }

    private IEnumerator FlashNegative()
    {
        danger.gameObject.SetActive(true);
        while (_currentHeat <= -1)
        {
            negativeFill.DOFade(0, FlashTime);
            danger.DOFade(0, FlashTime); 
            yield return new WaitForSeconds(FlashTime);
            negativeFill.DOFade(1, FlashTime);
            danger.DOFade(1, FlashTime); 
            yield return new WaitForSeconds(FlashTime);
        }
        negativeFill.DOFade(1, 0);
        danger.DOFade(1, 0);
        danger.gameObject.SetActive(false);
        _danger = false;
    }
    
    private IEnumerator FlashPositive()
    {
        speedUp.gameObject.SetActive(true);
        while (_currentHeat >= 1)
        {
            positiveFill.DOFade(0, FlashTime);
            speedUp.DOFade(0, FlashTime);
            yield return new WaitForSeconds(FlashTime);
            positiveFill.DOFade(1, FlashTime);
            speedUp.DOFade(1, FlashTime);
            yield return new WaitForSeconds(FlashTime);
        }
        positiveFill.DOFade(1, 0);
        speedUp.DOFade(1, 0);
        speedUp.gameObject.SetActive(false);
        _speedUp = false;
    }
    
    private void ChangeBackgroundColor()
    {
        background.color = Random.ColorHSV(0, 1, 0, 1, 0, 1, 1, 1);
    }
    
    private void ChangeBeat()
    {
        if (time.seconds - beatStartInSeconds < SecondsUntilBeatChange) return;
        beatStartInSeconds = time.seconds;

        if (_currentHeat >= 1)
        {
            ChangeProvider(30);
            _currentHeat = 0;
            positiveSlider.value = 0;
            speedUpSound.Play();
        }

        if (_currentHeat <= -1)
        {
            gameOverSound.Play();
            StopGame();
        }
        
        if (!_active) return;
        _style = audioClips.PlayRandomClip(providers[currentProviderIndex]);
        ChangeBackgroundColor();
    }
    
    private void SpawnStep()
    {
        if (!(GetDelta(_startTime, steps.spawnRate) >= 1)) return;
        steps.SpawnStep(_style);
        _startTime = Time.time;
    }
    
    private void ChangeProvider(int bmpChange)
    {
        if (currentProviderIndex + 1 >= providers.Count)
        {
            return;
        }
        currentProviderIndex++;
        ChangeBmp(bmpChange);
    }

    private void ChangeBmp(int bmpChange)
    {
        _currentBmp += bmpChange;
        steps.spawnRate = 1 / ((float)_currentBmp / 60);
        steps.stepDuration = (float)_currentBmp / 60;
        waveAnimator.speed = steps.stepDuration;
        AdjustBmpText();
    }

    private void AdjustBmpText()
    {
        bmpText.text = $"{_currentBmp} BMP";
        bmpText.color = Color.Lerp(Color.green, Color.red, (float)_currentBmp / 240);
    }
    
    public static float GetDelta(float startTime, float duration)
    {
        return (Time.time - startTime) / duration;
    }

    private void StopGame()
    {
        foreach (var manager in managers)
        {
            manager.Stop();
        }

        if (_currentScore > _currentTopScore)
        {
            PlayerPrefs.SetInt("TopScore", _currentScore);
            topScoreText.text = $"{_currentScore:0000000}";
        }
        _currentBmp = 60;
        _currentHeat = 0;
        _currentScore = 0;
        startPanel.SetActive(true);
        _active = false;
        var list = FindObjectsOfType<Step>();
        foreach (var step in list)
        {
            steps.ResetStep(step);
        }
        waveAnimator.speed = 1;
        positiveSlider.value =  0;
        negativeSlider.value = 0;
        scoreText.text = $"{_currentScore:0000000}";
        waveAnimator.enabled = false;
    }
}
