using System;
using System.Collections.Generic;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class AudioManager : Manager
{
    public AudioSource source;

    private string _lastClip = "";
    private Style _lastStyle = Style.None;

    private bool _active;
    public override void Init()
    {
        _active = true;
    }

    public Style PlayRandomClip(AudioProvider provider)
    {
        if(source.isPlaying) source.Stop();
        
        var (style, clip) = provider.GetRandomClip(_lastClip, _lastStyle);
        GameManager.Instance.ChangeStyle(style.ToString());
        _lastClip = clip.name;
        _lastStyle = style;
        source.clip = clip.audioClip;
        source.Play();
        return style;
    }
    
    public override void Stop()
    {
        source.Stop();
        _active = false;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(AudioManager))]
public class AudioManagerEditor : Editor
{
    private readonly List<string> _bmps = new()
    {
        "60BMP",
        "90BMP",
        "120BMP",
        "150BMP",
        "180BMP",
        "210BMP",
        "240BMP"
    };
    
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (!GUILayout.Button("Generate AudioProviders")) return;
        GenerateProviders();
    }

    private void GenerateProviders()
    {
        foreach (var bmp in _bmps)
        {
            var newProvider = CreateInstance<AudioProvider>();
            newProvider.bmp = bmp;
            foreach (var style in Enum.GetNames(typeof(Style)))
            {
                if(style == "None") continue;
                var styleClip = new StyleClip();
                styleClip.name = style;
                styleClip.style = Enum.Parse<Style>(style);
                styleClip.clips = new List<Clip>();
                int index = 0;
                foreach (var asset in AssetDatabase.FindAssets("*", new [] {$"Assets/Audio/{style}/{bmp}"}))
                {
                    if (!GUID.TryParse(asset, out var result)) continue;
                    var clip = new Clip
                    {
                        name = style + index
                    };
                    var path = AssetDatabase.GUIDToAssetPath(result);
                    clip.audioClip = AssetDatabase.LoadAssetAtPath<AudioClip>(path);
                    styleClip.clips.Add(clip);
                    index++;
                }
                newProvider.registeredClips.Add(styleClip);
            }
            
            AssetDatabase.CreateAsset(newProvider, $"Assets/Audio/{bmp}.asset");
        }
        AssetDatabase.SaveAssets();
    }
}
#endif
