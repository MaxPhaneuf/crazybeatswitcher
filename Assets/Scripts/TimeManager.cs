using TMPro;
using UnityEngine;

public class TimeManager : Manager
{
    private float _startTime;
    public int minutes, seconds;
    public TextMeshProUGUI text;
    private bool _active;
    
    public override void Init()
    {
        _startTime = Time.time;
        _active = true;
    }

    public override void Stop()
    {
        _active = false;
        seconds = 0;
        minutes = 0;
    }

    private void Update()
    {
        if (!_active) return;
        seconds = (int)(Time.time - _startTime);
        minutes = (int)((Time.time - _startTime) / 60);
        text.text = $"{minutes:00}:{seconds % 60:00}";
    }
}
