﻿using UnityEngine;

public class Step : MonoBehaviour
{
    public float startTime;
    public Vector3 startPos = new();
    public Vector3 targetPos = new();
    public float stepDuration;
    public StepDirection direction;
    public AudioSource missSound;
    public void Spawn(float travelDistance, float duration, StepDirection stepDirection)
    {
        startTime = Time.time;
        startPos = transform.position;
        stepDuration = duration;
        direction = stepDirection;
        targetPos = new Vector3(startPos.x, startPos.y - travelDistance, startPos.z);
    }

    public void RepositionWithDelta(float delta)
    {
        transform.position = Vector3.LerpUnclamped(startPos, targetPos,  delta);
    }
    
    public void RepositionWithDelta(float delta, Vector3 offset)
    {
        transform.position = Vector3.LerpUnclamped(startPos, targetPos + offset,  delta);
    }
}