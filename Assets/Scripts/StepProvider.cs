using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepProvider : Manager
{
    private const float StepYTravel = 9.25f;
    public Transform startPointLeft;
    public Transform startPointUp;
    public Transform startPointRight;
    public Transform startPointDown;
    
    public Transform poolTransform;

    public float stepDuration = 1;
    public float spawnRate = 1f;
    private float _startTime;

    private bool _active;

    public List<PatternProvider> patterns = new();
    private Dictionary<Style, PatternProvider> _registeredPatterns = new();
    private Dictionary<Style, int> _currentIndexes = new();

    private int stepPoolIndex;
    public override void Init()
    {
        _active = true;
        foreach (var pattern in patterns)
        {
            _registeredPatterns[pattern.style] = pattern;
            _currentIndexes[pattern.style] = 0;
        }
    }

    public void SpawnStep(Style style)
    {
        var index = _currentIndexes[style];
        var p = _registeredPatterns[style].patterns[index];

        foreach (var stepDirection in p.step)
        {
            if(stepDirection == StepDirection.None) continue;
            var startPoint = GetTransformWithDirection(stepDirection);
            var tr = transform.GetChild(stepPoolIndex);
            tr.position = startPoint.position;
            tr.rotation = startPoint.rotation;
            var step = tr.GetComponent<Step>();
            step.Spawn(StepYTravel, stepDuration, stepDirection);
            stepPoolIndex++;
            if (stepPoolIndex >= transform.childCount) stepPoolIndex = 0;
            StartCoroutine(LerpStep(step));
        }

        if (index + 1 >= _registeredPatterns[style].patterns.Count)
        {
            _currentIndexes[style] = 0;
        }
        else
        {
            _currentIndexes[style]++;
        }
    }

    private Transform GetTransformWithDirection(StepDirection direction)
    {
        return direction switch
        {
            StepDirection.Left => startPointLeft,
            StepDirection.Up => startPointUp,
            StepDirection.Right => startPointRight,
            StepDirection.Down => startPointDown,
            StepDirection.None => null,
            _ => null
        };
    }
    public override void Stop()
    {
        _active = false;
        stepDuration = 1;
        spawnRate = 1f;
        stepPoolIndex = 0;
    }
    
    private IEnumerator LerpStep(Step step)
    {
        float delta;
        bool goal = false;
        do
        {
            if(!_active) break;
            delta = GameManager.GetDelta(step.startTime, step.stepDuration);
            if(delta < 1f) step.RepositionWithDelta(delta);
            if (delta >= 1)
            {
                if (InputProvider.IsAxisPressed(step.direction))
                {
                    goal = true;
                    GameManager.Instance.ChangeScore();
                    break;
                }
                step.missSound.Play();
                GameManager.Instance.ChangeScore(true);
            }
            yield return new WaitForEndOfFrame();
        } while (delta < 1f);
        if (!goal && _active)
        {
            do
            {
                if (!_active) break;
                delta = GameManager.GetDelta(step.startTime, step.stepDuration);
                step.RepositionWithDelta(delta, Vector3.down * .25f);
                yield return new WaitForEndOfFrame();
            } while (delta < 1.25f);
        }

        if (_active)
            ResetStep(step);
    }

    public void ResetStep(Step step)
    {
        step.transform.position = poolTransform.position;
    }
}

public enum StepDirection
{
    Left,
    Up,
    Right,
    Down,
    None
}
