using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "AudioProvider", menuName = "Time Dancer/AudioProvider")]
public class AudioProvider : ScriptableObject
{
    public string bmp = "60BMP";
    public List<StyleClip> registeredClips = new();

    private Style GetRandomStyle(Style last)
    {
        var result = Style.None;
        do {
            result = (Style)Random.Range(0, Enum.GetNames(typeof(Style)).Length);
        } 
        while (result == last || GetClipsWithStyle(result).Count == 0);
        
        return result;
    }

    private List<Clip> GetClipsWithStyle(Style style)
    {
        foreach (var styleClip in registeredClips.Where(styleClip => styleClip.style == style))
        {
            return styleClip.clips;
        }
        return new List<Clip>();;
    }

    private static Clip GetRandomClip(string last, IReadOnlyList<Clip> list)
    {
        Clip result;
        do
        {
            result = list[Random.Range(0, list.Count)];
            
        } while (result.name == last);

        return result;
    }
    
    public (Style, Clip) GetRandomClip(string last, Style lastStyle)
    {
        var style = GetRandomStyle(lastStyle);
        var clips = GetClipsWithStyle(style);
        return (style, GetRandomClip(last, clips));
    }
}

[Serializable]
public class StyleClip
{
    public string name;
    public Style style;
    public List<Clip> clips;
}

[Serializable]
public class Clip
{
    public string name;
    public AudioClip audioClip;

    public override string ToString()
    {
        return name;
    }
}

public enum Style
{
    Bounce,
    Dissonance,
    Dnb,
    Edm,
    Garage,
    Grime,
    Horns,
    Minimal,
    Percussive,
    None
}