﻿using System;
using UnityEngine;

public class InputProvider : MonoBehaviour
{
    private static bool _isDownPressed;
    private static bool _isUpPressed;
    private static bool _isLeftPressed;
    private static bool _isRightPressed;

    private static bool _isDownQueried;
    private static bool _isUpQueried;
    private static bool _isLeftQueried;
    private static bool _isRightQueried;
    
    private void Update()
    {
        GetPressed();
    }

    private void GetPressed()
    {
        GetLeft();
        GetRight();
        GetUp();
        GetDown();

    }

    void GetLeft()
    {
        if (!_isLeftPressed && (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)))
            _isLeftPressed = true;
        
        if (_isLeftPressed && (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.A)))
        {
            _isLeftPressed = false;
            _isLeftQueried = false;
        }
    }
    
    void GetRight()
    {
        if (!_isRightPressed && (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)))
            _isRightPressed = true;
        
        if (_isRightPressed && (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.D)))
        {
            _isRightPressed = false;
            _isRightQueried = false;
        }
    }
    
    void GetUp()
    {
        if (!_isUpPressed && (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)))
            _isUpPressed = true;

        if (_isUpPressed && (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.W)))
        {
            _isUpPressed = false;
            _isUpQueried = false;
        }
            
    }
    
    void GetDown()
    {
        if (!_isDownPressed && (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)))
            _isDownPressed = true;

        if (_isDownPressed && (Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.S)))
        {
            _isDownPressed = false;
            _isDownQueried = false;
        }
    }
    
    public static bool IsAxisPressed(StepDirection direction)
    {
        switch (direction)
        {
            case StepDirection.Down when _isDownQueried:
                return false;
            case StepDirection.Down:
                _isDownQueried = true;
                return _isDownPressed;
            case StepDirection.Left when _isLeftQueried:
                return false;
            case StepDirection.Left:
                _isLeftQueried = true;
                return _isLeftPressed;
            case StepDirection.Up when _isUpQueried:
                return false;
            case StepDirection.Up:
                _isUpQueried = true;
                return _isUpPressed;
            case StepDirection.Right when _isRightQueried:
                return false;
            case StepDirection.Right:
                _isRightQueried = true;
                return _isRightPressed;
            case StepDirection.None:
                return false;
            default:
                return false;
        }
    }
}
