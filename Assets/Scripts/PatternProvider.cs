﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PatternProvider", menuName = "Time Dancer/PatternProvider", order = 0)]
public class PatternProvider : ScriptableObject
{
    public Style style;
    public List<Pattern> patterns = new();
}

[Serializable]
public class Pattern
{
    public List<StepDirection> step = new();
}

